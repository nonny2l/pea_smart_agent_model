﻿namespace RRPlatFormModel.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MasterModel
    {
     
        [StringLength(1)]
        public string IS_ACTIVE { get; set; }

        [StringLength(255)]
        public string CREATED_BY { get; set; }

        [StringLength(255)]
        public string UPDATED_BY { get; set; }

        public DateTime? CREATED_DATE { get; set; }

        public DateTime? UPDATED_DATE { get; set; }

    }
}
