namespace RRPlatFormModel.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class MT_MENU : MasterModel
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MENU_ID { get; set; }

        [StringLength(100)]
        public string NAME { get; set; }

        [StringLength(255)]
        public string DESCRIPTION { get; set; }

        [StringLength(255)]
        public string URL { get; set; }

        [StringLength(50)]
        public string ICON { get; set; }

        public int? SORT { get; set; }

        public int? PARENT_ID { get; set; }

        public int? MENU_TYPE_ID { get; set; }

        public int? MENU_GROUP_ID { get; set; }
    }
}
