namespace RRPlatFormModel.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class MT_USER_GROUP : MasterModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int USER_GROUP_ID { get; set; }

        [StringLength(40)]
        public string NAME { get; set; }

        [StringLength(100)]
        public string DESCRIPTION { get; set; }

    }
}
