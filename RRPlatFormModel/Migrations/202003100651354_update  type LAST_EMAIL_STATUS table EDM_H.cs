﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetypeLAST_EMAIL_STATUStableEDM_H : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.EDM_H", "LAST_EMAIL_STATUS", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.EDM_H", "LAST_EMAIL_STATUS", c => c.Int(nullable: false));
        }
    }
}
