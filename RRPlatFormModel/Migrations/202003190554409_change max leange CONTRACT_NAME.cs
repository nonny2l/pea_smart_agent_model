﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changemaxleangeCONTRACT_NAME : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CONTRACT_LIST", "CONTRACT_NAME", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CONTRACT_LIST", "CONTRACT_NAME", c => c.String(maxLength: 60));
        }
    }
}
