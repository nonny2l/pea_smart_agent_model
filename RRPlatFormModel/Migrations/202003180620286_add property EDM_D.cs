﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addpropertyEDM_D : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EDM_D", "ACTION_TYPE", c => c.String());
            AddColumn("dbo.EDM_D", "EMAIL", c => c.String());
            AddColumn("dbo.EDM_D", "SOURCE", c => c.String());
            AddColumn("dbo.EDM_D", "DEVICE", c => c.String());
            AddColumn("dbo.EDM_D", "SOURCE_TRANSECTION", c => c.String());
            AddColumn("dbo.EDM_D", "CAMPAINGN_ID", c => c.String());
            AddColumn("dbo.EDM_D", "CAMPAINGN_NAME", c => c.String());
            AddColumn("dbo.EDM_D", "DATE_TIME", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EDM_D", "DATE_TIME");
            DropColumn("dbo.EDM_D", "CAMPAINGN_NAME");
            DropColumn("dbo.EDM_D", "CAMPAINGN_ID");
            DropColumn("dbo.EDM_D", "SOURCE_TRANSECTION");
            DropColumn("dbo.EDM_D", "DEVICE");
            DropColumn("dbo.EDM_D", "SOURCE");
            DropColumn("dbo.EDM_D", "EMAIL");
            DropColumn("dbo.EDM_D", "ACTION_TYPE");
        }
    }
}
