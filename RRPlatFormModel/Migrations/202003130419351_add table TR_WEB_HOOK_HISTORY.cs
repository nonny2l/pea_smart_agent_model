﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtableTR_WEB_HOOK_HISTORY : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TR_WEB_HOOK_HISTORY",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        MESSAGE_ID = c.String(),
                        ACTION_TYPE = c.String(),
                        EMAIL = c.String(),
                        SOURCE = c.String(),
                        DEVICE = c.String(),
                        SOURCE_TRANSECTION = c.String(),
                        CAMPAINGN_ID = c.String(),
                        CAMPAINGN_NAME = c.String(),
                        DATE_TIME = c.String(),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TR_WEB_HOOK_HISTORY");
        }
    }
}
