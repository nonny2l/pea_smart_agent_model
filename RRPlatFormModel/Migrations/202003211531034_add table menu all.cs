﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtablemenuall : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MT_MENU",
                c => new
                    {
                        MENU_ID = c.Int(nullable: false, identity: true),
                        NAME = c.String(maxLength: 100),
                        DESCRIPTION = c.String(maxLength: 255),
                        URL = c.String(maxLength: 255),
                        ICON = c.String(maxLength: 50),
                        SORT = c.Int(),
                        PARENT_ID = c.Int(),
                        MENU_TYPE_ID = c.Int(),
                        MENU_GROUP_ID = c.Int(),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.MENU_ID);
            
            CreateTable(
                "dbo.MT_MENU_GROUP",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NAME = c.String(maxLength: 50),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MT_MENU_GROUP_PERMISSION",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        MENU_ID = c.Int(nullable: false),
                        GROUP_ID = c.Int(nullable: false),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.ID, t.MENU_ID, t.GROUP_ID });
            
            CreateTable(
                "dbo.MT_MENU_ROLE_PERMISSION",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        MENU_ID = c.Int(nullable: false),
                        ROLE_ID = c.Int(nullable: false),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.ID, t.MENU_ID, t.ROLE_ID });
            
            CreateTable(
                "dbo.MT_MENU_TYPE",
                c => new
                    {
                        MENU_TYPE_ID = c.Int(nullable: false, identity: true),
                        MENU_TYPE_NAME = c.String(maxLength: 255),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.MENU_TYPE_ID);
            
            CreateTable(
                "dbo.MT_MENU_TYPE_PERMISSION",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        MENU_ID = c.Int(nullable: false),
                        TYPE_ID = c.Int(nullable: false),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.ID, t.MENU_ID, t.TYPE_ID });
            
            CreateTable(
                "dbo.MT_USER_GROUP",
                c => new
                    {
                        USER_GROUP_ID = c.Int(nullable: false, identity: true),
                        NAME = c.String(maxLength: 40),
                        DESCRIPTION = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.USER_GROUP_ID);
            
            CreateTable(
                "dbo.MT_USER_GROUP_ROLE",
                c => new
                    {
                        USER_GROUP_ROLE_ID = c.Int(nullable: false, identity: true),
                        USER_ID = c.Int(nullable: false),
                        USER_GROUP_ID = c.Int(nullable: false),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.USER_GROUP_ROLE_ID, t.USER_ID, t.USER_GROUP_ID });
            
            CreateTable(
                "dbo.MT_USER_MENU_PERMISSION",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        USER_ID = c.Int(nullable: false),
                        MENU_ID = c.Int(nullable: false),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.ID, t.USER_ID, t.MENU_ID });
            
            CreateTable(
                "dbo.MT_USER_ROLE",
                c => new
                    {
                        ROLE_ID = c.Int(nullable: false, identity: true),
                        USER_ID = c.Int(nullable: false),
                        ROLE_NAME = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => new { t.ROLE_ID, t.USER_ID });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MT_USER_ROLE");
            DropTable("dbo.MT_USER_MENU_PERMISSION");
            DropTable("dbo.MT_USER_GROUP_ROLE");
            DropTable("dbo.MT_USER_GROUP");
            DropTable("dbo.MT_MENU_TYPE_PERMISSION");
            DropTable("dbo.MT_MENU_TYPE");
            DropTable("dbo.MT_MENU_ROLE_PERMISSION");
            DropTable("dbo.MT_MENU_GROUP_PERMISSION");
            DropTable("dbo.MT_MENU_GROUP");
            DropTable("dbo.MT_MENU");
        }
    }
}
