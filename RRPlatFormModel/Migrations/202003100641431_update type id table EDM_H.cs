﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetypeidtableEDM_H : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CONTRACT_LIST", "CUSTOMER_ID", c => c.Int(nullable: false));
            AlterColumn("dbo.EDM_H", "PK_GUN_ID", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.EDM_H", "PK_GUN_ID", c => c.Int(nullable: false));
            AlterColumn("dbo.CONTRACT_LIST", "CUSTOMER_ID", c => c.String(maxLength: 60));
        }
    }
}
