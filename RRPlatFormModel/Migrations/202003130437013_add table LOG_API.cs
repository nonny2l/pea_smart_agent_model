﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtableLOG_API : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LOG_API",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        REQUEST = c.String(unicode: false, storeType: "text"),
                        RESPONSE = c.String(unicode: false, storeType: "text"),
                        STATUS = c.String(maxLength: 50),
                        REQUEST_DATETIME = c.DateTime(),
                        RESPONSE_DATETIME = c.DateTime(),
                        MODULE_TYPE = c.String(maxLength: 100),
                        ACTION_TYPE = c.String(maxLength: 100),
                        USER_ACCOUNT = c.String(maxLength: 100),
                        IP = c.String(maxLength: 50),
                        CREATE_BY = c.String(maxLength: 30),
                        CREATE_DATE = c.DateTime(),
                        UPDATE_BY = c.String(maxLength: 30),
                        UPDATE_DATE = c.DateTime(),
                        DELETE_BY = c.String(maxLength: 30),
                        DELETE_DATE = c.DateTime(),
                        TRANSACTION_ID = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.LOG_API");
        }
    }
}
