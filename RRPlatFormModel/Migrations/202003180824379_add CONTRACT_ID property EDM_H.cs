﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCONTRACT_IDpropertyEDM_H : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EDM_H", "PK_TAXI_ID", c => c.String());
            AddColumn("dbo.EDM_H", "CONTRACT_ID", c => c.Int(nullable: false));
            AddColumn("dbo.EDM_H", "EMAIL", c => c.String());
            DropColumn("dbo.EDM_H", "PK_GUN_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EDM_H", "PK_GUN_ID", c => c.String());
            DropColumn("dbo.EDM_H", "EMAIL");
            DropColumn("dbo.EDM_H", "CONTRACT_ID");
            DropColumn("dbo.EDM_H", "PK_TAXI_ID");
        }
    }
}
