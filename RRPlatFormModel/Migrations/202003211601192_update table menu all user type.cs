﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetablemenuallusertype : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MT_USER_TYPE",
                c => new
                    {
                        USER_TYPE_ID = c.Int(nullable: false, identity: true),
                        USER_TYPE_NAME = c.String(maxLength: 255),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.USER_TYPE_ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MT_USER_TYPE");
        }
    }
}
