﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMAIN_FROM_STATUSpropertyCONTRACT_LIST : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CONTRACT_LIST", "MAIN_FROM_STATUS", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CONTRACT_LIST", "MAIN_FROM_STATUS");
        }
    }
}
