﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetablemenuall : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MT_USER_GROUP", "IS_ACTIVE", c => c.String(maxLength: 1));
            AddColumn("dbo.MT_USER_GROUP", "CREATED_BY", c => c.String(maxLength: 255));
            AddColumn("dbo.MT_USER_GROUP", "UPDATED_BY", c => c.String(maxLength: 255));
            AddColumn("dbo.MT_USER_GROUP", "CREATED_DATE", c => c.DateTime());
            AddColumn("dbo.MT_USER_GROUP", "UPDATED_DATE", c => c.DateTime());
            AddColumn("dbo.MT_USER_ROLE", "IS_ACTIVE", c => c.String(maxLength: 1));
            AddColumn("dbo.MT_USER_ROLE", "CREATED_BY", c => c.String(maxLength: 255));
            AddColumn("dbo.MT_USER_ROLE", "UPDATED_BY", c => c.String(maxLength: 255));
            AddColumn("dbo.MT_USER_ROLE", "CREATED_DATE", c => c.DateTime());
            AddColumn("dbo.MT_USER_ROLE", "UPDATED_DATE", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MT_USER_ROLE", "UPDATED_DATE");
            DropColumn("dbo.MT_USER_ROLE", "CREATED_DATE");
            DropColumn("dbo.MT_USER_ROLE", "UPDATED_BY");
            DropColumn("dbo.MT_USER_ROLE", "CREATED_BY");
            DropColumn("dbo.MT_USER_ROLE", "IS_ACTIVE");
            DropColumn("dbo.MT_USER_GROUP", "UPDATED_DATE");
            DropColumn("dbo.MT_USER_GROUP", "CREATED_DATE");
            DropColumn("dbo.MT_USER_GROUP", "UPDATED_BY");
            DropColumn("dbo.MT_USER_GROUP", "CREATED_BY");
            DropColumn("dbo.MT_USER_GROUP", "IS_ACTIVE");
        }
    }
}
