﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCUSTOMER_NOpropertyCONTRACT_LIST : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CONTRACT_LIST", "CUSTOMER_NO", c => c.String(maxLength: 60));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CONTRACT_LIST", "CUSTOMER_NO");
        }
    }
}
