﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addnewpropertycustomerandcontact : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CUSTOMER_PROFILE", "ZONE", c => c.String(maxLength: 10));
            AddColumn("dbo.CUSTOMER_PROFILE", "BUSSINESS_TYPE", c => c.String(maxLength: 255));
            AlterColumn("dbo.CONTRACT_LIST", "POSITION_NAME", c => c.String(maxLength: 255));
            AlterColumn("dbo.CUSTOMER_PROFILE", "SUB_BUSSINESS_TYPE", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CUSTOMER_PROFILE", "SUB_BUSSINESS_TYPE", c => c.String(maxLength: 100));
            AlterColumn("dbo.CONTRACT_LIST", "POSITION_NAME", c => c.String(maxLength: 200));
            DropColumn("dbo.CUSTOMER_PROFILE", "BUSSINESS_TYPE");
            DropColumn("dbo.CUSTOMER_PROFILE", "ZONE");
        }
    }
}
