﻿// <auto-generated />
namespace RRPlatFormModel
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class addMAIN_FROM_STATUSpropertyCONTRACT_LIST : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addMAIN_FROM_STATUSpropertyCONTRACT_LIST));
        
        string IMigrationMetadata.Id
        {
            get { return "202003180631318_add MAIN_FROM_STATUS property CONTRACT_LIST"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
