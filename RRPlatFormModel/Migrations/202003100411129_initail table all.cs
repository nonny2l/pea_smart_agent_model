﻿namespace RRPlatFormModel
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initailtableall : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ACTIVITY_CUSTOMER_HISTORY",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CUSTOMER_ID = c.Int(nullable: false),
                        APPOINTMENT_DATE = c.DateTime(),
                        ADDRESS_SURVEY = c.String(maxLength: 1000),
                        VENDER_NAME = c.String(maxLength: 200),
                        VENDER_CODE = c.String(maxLength: 20),
                        STATUS = c.Int(nullable: false),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CONTRACT_LIST",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CUSTOMER_ID = c.String(maxLength: 60),
                        CONTRACT_NAME = c.String(maxLength: 60),
                        EMAIL = c.String(maxLength: 100),
                        TEL = c.String(maxLength: 10),
                        POSITION_NAME = c.String(maxLength: 200),
                        GROUP_CODE = c.String(maxLength: 10),
                        CHANEL = c.String(),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CUSTOMER_PROFILE",
                c => new
                    {
                        CUSTOMER_ID = c.Int(nullable: false, identity: true),
                        CUSTOMER_NO = c.String(maxLength: 60),
                        CA_NO = c.String(maxLength: 60),
                        CUSTOMER_NAME = c.String(maxLength: 1000),
                        ADDRESS = c.String(maxLength: 4000),
                        SUBDISTRICT = c.String(maxLength: 100),
                        DISTRICT = c.String(maxLength: 100),
                        PROVINCE = c.String(maxLength: 100),
                        REGION = c.String(maxLength: 100),
                        TEL01 = c.String(maxLength: 10),
                        TEL02 = c.String(maxLength: 10),
                        TEL03 = c.String(maxLength: 10),
                        AVERAGE_AMOUNT_OF_ELECTRICCITY_USAGE = c.String(),
                        SUMMARY_AMOUNT_OF_ELECTRICCITY_USAGE = c.String(),
                        SUB_BUSSINESS_TYPE = c.String(maxLength: 100),
                        ACTIVITY_STATUS = c.Int(nullable: false),
                        URL = c.String(maxLength: 4000),
                        QR_CODE = c.String(unicode: false, storeType: "text"),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.CUSTOMER_ID);
            
            CreateTable(
                "dbo.EDM_D",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        EDM_H_ID = c.Int(nullable: false),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EDM_H",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CA_NO = c.String(maxLength: 60),
                        CUSTOMER_ID = c.Int(nullable: false),
                        PK_GUN_ID = c.Int(nullable: false),
                        SEND_DATE = c.DateTime(),
                        EMD_TYPE = c.Int(nullable: false),
                        LAST_EMAIL_STATUS = c.Int(nullable: false),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.LIST_GOOGLE_FORM",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CAMPIAN_CODE = c.Int(nullable: false),
                        CAMPIAN_DATE = c.DateTime(),
                        VENDER_CODE = c.String(maxLength: 20),
                        VENDER_NAME = c.String(maxLength: 200),
                        VENDER_TEL = c.String(maxLength: 20),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MT_CUSTOMER_STATUS",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        STATUS_NAME = c.String(maxLength: 20),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MT_EDM_TYPE",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        STATUS_NAME = c.String(maxLength: 20),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MT_USER",
                c => new
                    {
                        USER_ID = c.Int(nullable: false, identity: true),
                        USERNAME = c.String(maxLength: 60),
                        PASSWORD = c.String(maxLength: 60),
                        USER_GROUP_ID = c.Int(),
                        REMEMBER_TOKEN = c.String(maxLength: 100),
                        USER_TYPE_ID = c.Int(nullable: false),
                        EMP_NO = c.String(maxLength: 10),
                        AGENT_NO = c.String(maxLength: 10),
                        NAME = c.String(maxLength: 255),
                        AVATAR = c.String(maxLength: 255),
                        EMAIL = c.String(maxLength: 255),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.USER_ID);
            
            CreateTable(
                "dbo.SOURCE_DATA",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CUSTOMER_ID = c.Int(nullable: false),
                        SOURCE_NAME = c.String(maxLength: 1000),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TR_APPOINTMENT",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CUSTOMER_ID = c.Int(nullable: false),
                        APPOINTMENT_DATE = c.DateTime(),
                        ADDRESS_SURVEY = c.String(maxLength: 1000),
                        VENDER_NAME = c.String(maxLength: 200),
                        VENDER_CODE = c.String(maxLength: 20),
                        STATUS = c.Int(nullable: false),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TR_FOLLOW",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CUSTOMER_ID = c.Int(nullable: false),
                        APPOINTMENT_DATE = c.DateTime(),
                        ADDRESS_SURVEY = c.String(maxLength: 1000),
                        VENDER_NAME = c.String(maxLength: 200),
                        VENDER_CODE = c.String(maxLength: 20),
                        STATUS = c.Int(nullable: false),
                        IS_ACTIVE = c.String(maxLength: 1),
                        CREATED_BY = c.String(maxLength: 255),
                        UPDATED_BY = c.String(maxLength: 255),
                        CREATED_DATE = c.DateTime(),
                        UPDATED_DATE = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TR_FOLLOW");
            DropTable("dbo.TR_APPOINTMENT");
            DropTable("dbo.SOURCE_DATA");
            DropTable("dbo.MT_USER");
            DropTable("dbo.MT_EDM_TYPE");
            DropTable("dbo.MT_CUSTOMER_STATUS");
            DropTable("dbo.LIST_GOOGLE_FORM");
            DropTable("dbo.EDM_H");
            DropTable("dbo.EDM_D");
            DropTable("dbo.CUSTOMER_PROFILE");
            DropTable("dbo.CONTRACT_LIST");
            DropTable("dbo.ACTIVITY_CUSTOMER_HISTORY");
        }
    }
}
