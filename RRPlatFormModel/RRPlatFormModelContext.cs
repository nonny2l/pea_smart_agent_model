namespace RRPlatFormModel
{
    using RRPlatFormModel.Models;
    using System.Data.Entity;

    public partial class RRSmartAgentModelContext : DbContext
    {
        public RRSmartAgentModelContext()
          : base("name=RRSmartAgentModel")
        {

        }

        public DbSet<MT_USER> MT_USER { get; set; }
        public DbSet<ACTIVITY_CUSTOMER_HISTORY> ACTIVITY_CUSTOMER_HISTORY { get; set; }
        public DbSet<CUSTOMER_PROFILE> CUSTOMER_PROFILE { get; set; }
        public DbSet<EDM_D> EDM_D { get; set; }
        public DbSet<EDM_H> EDM_H { get; set; }
        public DbSet<LIST_GOOGLE_FORM> LIST_GOOGLE_FORM { get; set; }
        public DbSet<MT_CUSTOMER_STATUS> MT_CUSTOMER_STATUS { get; set; }
        public DbSet<MT_EDM_TYPE> MT_EDM_TYPE { get; set; }
        public DbSet<SOURCE_DATA> SOURCE_DATA { get; set; }
        public DbSet<TR_APPOINTMENT> TR_APPOINTMENT { get; set; }
        public DbSet<TR_FOLLOW> TR_FOLLOW { get; set; }
        public DbSet<CONTRACT_LIST> CONTRACT_LIST { get; set; }
        public DbSet<TR_WEB_HOOK_HISTORY> TR_WEB_HOOK_HISTORY { get; set; }
        public DbSet<LOG_API> LOG_API { get; set; }

        public virtual DbSet<MT_MENU> MT_MENU { get; set; }
        public virtual DbSet<MT_MENU_TYPE> MT_MENU_TYPE { get; set; }
        public virtual DbSet<MT_MENU_GROUP> MT_MENU_GROUP { get; set; }
        public virtual DbSet<MT_MENU_GROUP_PERMISSION> MT_MENU_GROUP_PERMISSION { get; set; }
        public virtual DbSet<MT_USER_GROUP> MT_USER_GROUP { get; set; }
        public virtual DbSet<MT_USER_GROUP_ROLE> MT_USER_GROUP_ROLE { get; set; }
        public virtual DbSet<MT_USER_MENU_PERMISSION> MT_USER_MENU_PERMISSION { get; set; }
        public virtual DbSet<MT_MENU_ROLE_PERMISSION> MT_MENU_ROLE_PERMISSION { get; set; }
        public virtual DbSet<MT_USER_ROLE> MT_USER_ROLE { get; set; }
        public virtual DbSet<MT_MENU_TYPE_PERMISSION> MT_MENU_TYPE_PERMISSION { get; set; }
        public virtual DbSet<MT_USER_TYPE> MT_USER_TYPE { get; set; }


        //public virtual DbSet<TEST_TABLE> TEST_TABLE { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
